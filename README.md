<h2>Harfix builder</h2><br />
Script is licenced under GNU GPL v2 <br />
 <br />
Script makes compiling of kernel more confortable with some usefull features.  <br />
 <br />
<h3> Instruction </h3>
 - Move build.sh to root folder of Your kernel <br />
 - Open script with text editor <br />
 - Edit values to yours in table <br />
 - Grant script exec permission <br />
 - Run terminal in kernel folder and exec script ./build.sh <br />
 <br />

Officially used in: Harfix2, Harfix3, Anarchy, Harfix4,Harfix Kubix, Harfix5, Harfix6, Cherry <br />
